import NextAuth from "next-auth"
import Providers from "next-auth/providers"
import connect from "../../../utils/database"
import { compare, hash } from 'bcryptjs';

const options = {

  session: {
    jwt: true,
  },


  providers: [

    Providers.Credentials({

      credentials: {
        email: { label: "email", type: "text", placeholder: "jsmith" },
        password: { label: "password", type: "password" }
      },

      async authorize(credentials) {

        const { db } = await connect();


        const result = await db.collection('user').findOne({
          email: credentials.email,
          password: credentials.password
        });

        if (!result) {
          throw new Error('Password doesnt match');
        } else {
          return result
        }

      }
    })
  ], callbacks: {

    jwt(token, profile) {
      if (profile) {
        return { ...token, user: profile }
      }
      return token
    },
    session(session, token) {
      return token
    }

  },
}

export default (req, res) => NextAuth(req, res, options);
