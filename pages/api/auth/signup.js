import connect from '../../../utils/database';

async function handler(req, res) {
    //Only POST mothod is accepted
    if (req.method === 'POST') {

        const { db } = await connect();

        //Getting email and password from body
        const { email, password } = req.body;
        //Validate
        if (!email || !email.includes('@') || !password) {
            res.status(422).json({ message: 'Invalid Data' });
            return;
        }
        const checkExisting = await db
        .collection('user')
        .find({'email': email}).toArray()
        
        //Send error response if duplicate user is found
        if (checkExisting.length) {
            res.status(200).json(false);
            return;
        }
        
        //Hash password
        const status = await db.collection('user').insertOne({
            email,
            password
        });
        //Send success response
        res.status(201).json({ message: 'User created', ...status });
        //Close DB connection
    } else {
        //Response for other than POST method
        res.status(500).json({ message: 'Route not valid' });
    }
}

export default handler;