import connect from "../../utils/database"

export default async function login(req, res) { 
    if (req.method == 'POST') {

        const {email, password} = req.body;

        const { db } = await connect(); 

        const userExist = await db.collection('user').find({'email': email, 'password': password}).toArray()

        if (!userExist.length) {
            res.status(200).json(false);
            return;
        } else {
            res.status(200).json(true);
        }

    }
}