import React, { Component } from "react";
import Navbar from "./Navbar";

class Hero extends React.Component {
    render() {
        return (
            <>
                <div className="pt-24">
                    <div className="container px-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
                        <div className="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left">
                            <p className="font-tt uppercase tracking-loose w-full">Dificil encontrar emprego ?</p>
                            <h1 className="my-4 text-5xl font-bold leading-tight">
                                Faça aqui o seu curriculo
                            </h1>
                            <p className="leading-normal text-2xl mb-8">
                                De maneira rapida e facil e com os melhores modelos
                            </p>
                            <button className="font-tt mx-auto lg:mx-0 hover:underline bg-white text-gray-800 font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
                                Log in
                            </button>
                        </div>
                        <div className="w-full md:w-3/5 py-6 text-center">
                            <img className="w-full md:w-4/5 z-50" src="linkedin.png" />
                        </div>
                    </div>
                </div>
                <section className="bg-white border-b py-8">
                    <div className="container mx-auto flex flex-wrap pt-4 pb-12">
                        <h1 className="w-full my-2 text-5xl font-bold leading-tight text-center text-gray-800">
                            Time
                        </h1>
                        <div className="w-full mb-4">
                            <div className="h-1 mx-auto gradient w-64 opacity-25 my-0 py-0 rounded-t"></div>
                        </div>
                        <div className="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                            <div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                                <a href="#" className="flex flex-wrap no-underline hover:no-underline">
                                    <p className="w-full text-black text-xs md:text-xl px-6">
                                        Ryan Temoteo
                                    </p>
                                    <div>
                                        <img className="mt-2 rounded w-auto h-auto" src="ryan.png" />
                                    </div>
                                    <div className="mt-3 w-full font-bold text-xl text-gray-800 px-6">
                                        O estagiario mais antigo
                                    </div>
                                    <p className="mt-1 text-gray-800 text-base px-6 mb-5">
                                        Criado no fundo dos Campos Eliseos Ryan Temoteo veio para representar no front deste aplicação
                                    </p>
                                </a>
                            </div>
                            <div className="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                                <div className="flex items-center justify-start">
                                    <button className="mx-auto lg:mx-0 hover:underline gradient text-black font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
                                        Git
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                            <div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                                <a href="#" className="flex flex-wrap no-underline hover:no-underline">
                                    <p className="w-full text-black text-xs md:text-xl px-6">
                                        Eduardo Longhi
                                    </p>
                                    <div>
                                        <img className="mt-2 rounded w-auto h-auto" src="eduardo.png" />
                                    </div>
                                    <div className="mt-3 w-full font-bold text-xl text-gray-800 px-6">
                                        O cabo Longhi
                                    </div>
                                    <p className="mt-1 text-gray-800 text-base px-6 mb-5">
                                        Diretamente de Mogi Mirim veio ser o full-stack ninja da aplicação
                                    </p>
                                </a>
                            </div>
                            <div className="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                                <div className="flex items-center justify-start">
                                    <button className="mx-auto lg:mx-0 hover:underline gradient text-black font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
                                        Git
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="w-full md:w-1/3 p-6 flex flex-col flex-grow flex-shrink">
                            <div className="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
                                <a href="#" className="flex flex-wrap no-underline hover:no-underline">
                                    <p className="w-full text-black text-xs md:text-xl px-6">
                                        Eyko Nathan
                                    </p>
                                    <div>
                                        <img className="mt-2 rounded w-auto h-auto" src="eyko.png" />
                                    </div>
                                    <div className="mt-3 w-full font-bold text-xl text-gray-800 px-6">
                                        O Genro
                                    </div>
                                    <p className="mt-1 text-gray-800 text-base px-6 mb-5">
                                        Diretamente de algum lugar onde não tenho o previo conhecimento, veio para formar uma incrivel dupla Ryan para o front da aplicação
                                    </p>
                                </a>
                            </div>
                            <div className="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
                                <div className="flex items-center justify-start">
                                    <button className="mx-auto lg:mx-0 hover:underline gradient text-black font-bold rounded-full my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
                                        Git
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Hero;