import React, { Component } from "react"
import { signin, signIn } from "next-auth/client";

class Nologin extends React.Component {

    render() {
    return (
            <div className=" bg-blue-600 lg:min-h-screen lg:flex lg:items-center p-8 sm:p-12">
                <div className="flex-grow">
                    <h1 className="text-white text-center text-2xl sm:text-5xl mb-2">
                        Você não esta logado
                    </h1>
                    <p className="text-center text-blue-200 sm:text-lg">
                        Faça aqui o seu login :D
                    </p>
                    <p className="text-center">
                        <button onClick={signin}> Log In </button>
                    </p>
                </div>
            </div>
    
    )}
} 
export default Nologin;
  

