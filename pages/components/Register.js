import  { React, useState } from "react";
import axios from "axios";
import { signIn } from "next-auth/client";

const Register = () => {

    const [ data, setData ] = useState({
        email: "",
        password: "",
        loading: false

    })

    function handler(e) {
        const newData={ ...data };
        newData[e.target.id] = e.target.value;
        setData(newData);
      }
    
    const onFormSubmit = async (e) => {
        e.preventDefault();

        if (!data.email || !data.email.includes('@') || !data.password) {
            alert('Invalid details');
            return;
        }
        //POST form values
        await axios.post('/api/auth/signup',{
        email: data.email,
        password: data.password
        })
        .then( res => {
            if(!res.data) {
                alert('Usuario já cadastrado');
            } else {
                window.location.href = signIn()
            }
        } )
    };

    return (
        <div className="lg:min-h-screen lg:flex lg:items-center p-12 lg:p-24 xl:p-48">
            {}
            <div className="flex-grow bg-white shadow-xl rounded-md border border-gray-300 p-8">
                <div className="sm:flex sm:items-center">
                    <form onSubmit={(e) => onFormSubmit(e)}>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">Email</label>                 
                                <input 
                                    onChange={(e) => handler(e)} 
                                    id='email'
                                    value={data.email}
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    placeholder='Email'
                                    required />
                        </div>
                        <div className='mb-6'>
                            <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password"> Password </label>
                                <input 
                                    onChange={(e) => handler(e)}
                                    id='email'
                                    value={data.password}
                                    className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                    id='password'
                                    placeholder='Password' 
                                    type='password' 
                                    required />
                        </div>
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit"
                            >
                            CADASTRAR-SE
                        </button>
                    </form>
                </div>
                    <a className="mt-20 text-center text-blue-500" href="/">Já tem cadastro ?</a>
            </div> 
        </div>
        )
    }

export default Register;