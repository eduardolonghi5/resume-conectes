import Login from "./components/Login";

export default function Home() {
    return (
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div className=" bg-blue-600 lg:min-h-screen lg:flex lg:items-center p-8 sm:p-12">
          <div className="flex-grow">
            <h1 className="text-white text-center text-2xl sm:text-5xl mb-2">
                Olá seja bem-vindo(a)
            </h1>
            <p className="text-center text-blue-200 sm:text-lg">
                Faça aqui o seu login :D
            </p>
          </div>
        </div>
        <Login />
      </div>
  
    )
  }
  
  