import React from "react";
import { signIn, signOut, useSession } from "next-auth/client";

export default function Page() {
  const [session, loading] = useSession();

  console.log(session)

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    
    <>
      {session ? (
        <p>
        <button onClick={signOut}>Sign out</button>
        <p>Super secret page!</p>
        </p>
        
      ) : (
        <p>
          <p>You are not permitted to see this page.</p>
          <button onClick={signIn}>Sign in</button>
        </p>
      )}
    </>
  );
}