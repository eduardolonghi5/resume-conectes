import Nologin from "./components/Nologin"
import { getSession, signIn, signOut, useSession } from "next-auth/client";


export default function teste() {
    const [session] = useSession();


    return (

        <>
          {session ? (
            <p>
            <button onClick={signOut}>Sign out</button>
          
            <p>Super secret page! {session.user.email} </p>
            </p>
            
          ) : (
            <Nologin />
          )}
        </>
      );

}

signIn.getInitialProps = async(context) => {
  const {req, res} = context;
  const session = await getSession({req});
  
}