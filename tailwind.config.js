module.exports = {
  purge: ["./components/**/*.{js,ts,jsx,tsx}", "./pages/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'sign-blue': '#2196f3',
        'graybg': '#f2f5fa',
        'blueHead': '#363e96',
        
      },
      fontFamily: {
        'tt': 'TT Commons W01'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
  "postcss-import",
  "tailwindcss",
  "autoprefixer"],
}
